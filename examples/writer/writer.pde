import slideshow.*;

class Slide01 extends ThemedSlide {
   private Writer title;
   Slide01(int width_, int height_, Theme theme_, PApplet parent_) {
      super(width_, height_, theme_, parent_);
      Style textStyle = new Style(theme_.titleStyle);

      title = new Writer(textStyle, 0.1*width_, 0.4*height_, 0.8*width_, 0.6*height_, parent_);
      addItem(title);

      title.addTextLine("Slideshow Writer");
   }
}

class Slide02 extends ThemedSlide {
   private Writer textArea;
   Slide02(int width_, int height_, Theme theme_, PApplet parent_) {
      super(width_, height_, theme_, parent_);
      setTitle("Features");
      Style textStyle = new Style(theme.textStyle);

      textArea = new Writer(textStyle, 0.1*width_, 0.2*height_, 0.8*width_, 0.7*height_, parent_);
      addItem(textArea);

      textArea.addTextLine("It is a class to render generic text.");
      textArea.addTextLine("It can cope with long lines by measuring how many pixels each word measures and introducing linebreaks when necessary.");

      textArea.addTextLine("In addition to left justification it can");
      textArea.setAlign(Align.RIGHT);
      textArea.addTextLine("justify text to the right");
      textArea.setAlign(Align.CENTER);
      textArea.addTextLine("and even center it!");
   }
}

class Slide03 extends ThemedSlide {
   private Writer textArea;
   Slide03(int width_, int height_, Theme theme_, PApplet parent_) {
      super(width_, height_, theme_, parent_);
      setTitle("Features");
      Style textStyle = new Style(theme.textStyle);

      textArea = new Writer(textStyle, 0.1*width_, 0.2*height_, 0.8*width_, 0.7*height_, parent_);
      addItem(textArea);


      textArea.setAlign(Align.LEFT);
      textArea.addTextLine("Nicely typeset paragraphs can be displayed with first-line indentation, that distinguishes the begining of the paragraphs.");
      textArea.firstIndent(0.05*width_);
      textArea.addTextLine("It is easy to see that this is a different paragraph due to the indentation of the first line.");

      textArea.indent(0.05*width_);
      textArea.addTextLine("Also whole paragraphs can be indented, to give them a special character that makes them stand out from the bulk of the text.");
      textArea.unIndent();
   }
}

class Slide04 extends ThemedSlide {
   private Writer textArea;
   Slide04(int width_, int height_, Theme theme_, PApplet parent_) {
      super(width_, height_, theme_, parent_);
      setTitle("Features");
      Style textStyle = new Style(theme.textStyle);

      textArea = new Writer(textStyle, 0.1*width_, 0.2*height_, 0.8*width_, 0.7*height_, parent_);
      addItem(textArea);


      textArea.addTextLine("But maybe the most important feature for creating presentations is:");
      textArea.indent(0.1*width_);
      textArea.firstIndent(-0.05*width_);
      textArea.prefix("• ");
      textArea.addTextLine("The itemize feature.");
      textArea.addTextLine("They are often used this in presentations because:");
      textArea.indent(0.1*width_);
      textArea.addTextLine("Support oral disertations with key points.");
      textArea.addTextLine("Are an easy default in most software.");
      textArea.unIndent();
      textArea.addTextLine("It is easy to close one level.");
      textArea.unIndent();
      textArea.prefix("");
   }
}

class Slide05 extends ThemedSlide {
   private Writer textArea;
   Slide05(int width_, int height_, Theme theme_, PApplet parent_) {
      super(width_, height_, theme_, parent_);
      setTitle("Features");
      Style textStyle = new Style(theme.textStyle);

      textArea = new Writer(textStyle, 0.1*width_, 0.2*height_, 0.8*width_, 0.7*height_, parent_);
      addItem(textArea);

      textArea.addTextLine("Character level formatting can also be achieved:");
      textArea.indent(0.1*width_);
      textArea.firstIndent(-0.05*width_);
      textArea.prefix("• ");
      textArea.addText("Like setting a");
      textArea.bold();
      textArea.addText("bold");
      textArea.unBold();
      textArea.addTextLine("word.");
      textArea.addText("Or writting");
      textArea.italic();
      textArea.addText("something");
      textArea.unItalic();
      textArea.addTextLine("in italic.");
      textArea.addText("The");
      float size = textStyle.getSize();
      textStyle.setSize(size*0.5);
      textArea.addText("font");
      textStyle.setSize(size*0.75);
      textArea.addText("size");
      textStyle.setSize(size*1.5);
      textArea.addText("can");
      textStyle.setSize(size*2.0);
      textArea.addText("be");
      textStyle.setSize(size);
      textArea.addTextLine("changed.");
      textArea.addText("Also");
      String[] message = {"the","color","of","the","text"};
      color c1=0xffffbc42;
      color c2=0xff8f2d56;
      color base = textStyle.getColor();
      for(int i=0; i < message.length; i++) {
         textStyle.setColor(lerpColor(c1,c2,(float)i/message.length));
         textArea.addText(message[i]);
      }
      textStyle.setColor(base);
      textArea.addTextLine(".");
      String[] icons = {"pacman.png","dot.png","dot.png","dot.png","dot.png","dot.png","dot.png","ghost1.png","ghost2.png","ghost3.png","ghost4.png"};
      textArea.addText("Last but not least:");
      for(int i=0; i < icons.length; i++) {
         PImage icon = loadImage(icons[i]); 
         icon.resize(0, 60); 
         textArea.addImage(icon);
      }
      textArea.addTextLine();
      textArea.unIndent();
      textArea.prefix("");
   }
}

class Slide06 extends ThemedSlide {
   private Writer textArea,textArea1;
   Slide06(int width_, int height_, Theme theme_, PApplet parent_) {
      super(width_, height_, theme_, parent_);
      setTitle("Features");
      Style textStyle = new Style(theme.textStyle);

      textArea = new Writer(textStyle, 0.1*width_, 0.2*height_, 0.8*width_, 0.7*height_, parent_);
      addItem(textArea);

      textArea.prefix("• ");
      textArea.addTextLine("Animations are also possible.", new AnimationGroup());
      String[] stageMessage = {"With","text","that","appears","and","dissapears."};
      int i=2;
      for(int j=0; j < stageMessage.length; j++) {
         textArea.addText(stageMessage[j], new Stage(i,++i));
      }
      textArea.addTextLine();
      String[] fadeMessage = { "Or","text","that","graciously","fades","in","and","out." };
      for(int j=0; j < fadeMessage.length; j++) {
         textArea.addText(fadeMessage[j], new AnimationGroup(new Shift(i,1500,0,0,0,1000),new Opacity(++i,0,1,1000), new Opacity(++i,1,0.2,1000)));
      }
      textArea.addTextLine();
      String[] shiftMessage = { "And","text","that","arrives","from","different","points","in","space." };
      for(int j=0; j < shiftMessage.length; j++) {
         textArea.addText(shiftMessage[j], new Shift(i++,lerp(400,-400,(float)j/shiftMessage.length),-700,0,0,1000));
      }
      textArea.addTextLine();

       
   }
}

SlideShow theSlideShow;

void setup() {
   fullScreen(1);
   theSlideShow = new SlideShow(this);

   Theme theTheme = new Theme();
   theTheme.titleStyle = new Style(
         createFont("Roboto-Regular.ttf",120),
         createFont("Roboto-Bold.ttf",120),
         createFont("Roboto-Italic.ttf",120),
         createFont("Roboto-BoldItalic.ttf",120),
         120.0,0xff2f2f2f);
   theTheme.textStyle = new Style(
         createFont("Roboto-Regular.ttf",60),
         createFont("Roboto-Bold.ttf",60),
         createFont("Roboto-Italic.ttf",60),
         createFont("Roboto-BoldItalic.ttf",60),
         60.0,0xff2f2f2f);
   theTheme.background = loadImage("light.jpg");

   theSlideShow.addSlide(new Slide01(width, height, theTheme, this));
   theSlideShow.addSlide(new Slide02(width, height, theTheme, this));
   theSlideShow.addSlide(new Slide03(width, height, theTheme, this));
   theSlideShow.addSlide(new Slide04(width, height, theTheme, this));
   theSlideShow.addSlide(new Slide05(width, height, theTheme, this));
   theSlideShow.addSlide(new Slide06(width, height, theTheme, this));
}
int i =0;
void draw() {
  theSlideShow.draw(); 
  println(i++);
}

synchronized void keyPressed() {
   theSlideShow.keyPressed();
}
