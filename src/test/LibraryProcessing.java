package test;

import java.io.File;

import processing.core.PApplet;
import slideshow.Align;
import slideshow.SlideShow;
import slideshow.Style;
import slideshow.Theme;
import slideshow.ThemedSlide;
import slideshow.Writer;

public class LibraryProcessing extends PApplet {
   private static SlideShow theSlideShow = new SlideShow();
   private static float width;
   private static float height;
   private static Theme theme;

   @Override
   public void settings() {
      fullScreen();
   }

   @Override
   public void setup() {
      theSlideShow.addparent(this);
      width = 1080;
      height = 1920;
      theme = new Theme();
      theme.textStyle = new Style(
            this.createFont("Roboto-Regular.ttf",60),
            this.createFont("Roboto-Bold.ttf",60),
            this.createFont("Roboto-Italic.ttf",60),
            this.createFont("Roboto-BoldItalic.ttf",60),
            (float) 40.0,0xffc5d492);
      theme.background = loadImage("dark.jpg"); 
      theSlideShow.addSlide(new MySlide((int)width, (int)height, theme,this));
   }

   @Override
   public void draw() {
      theSlideShow.draw();
      this.save(File.separator+"tmp"+ File.separator+"test.jpg");
      exit();	
   }

   public static void main(String[] args) {
      PApplet.main(new String[] { "test.LibraryProcessing"});
   }
}

class MySlide extends ThemedSlide {
   Writer textArea;
   MySlide(int width_, int height_,Theme theme_, PApplet sketch_) {
	   super(width_, height_, theme_, sketch_);
	      Style textStyle = new Style(theme.textStyle);
	      
	      textArea = new Writer(textStyle, (float)0.1*width_, (float)0.1*height_, (float)0.8*width_, (float)0.7*height_, sketch_);
	      addItem(textArea);

	      textArea.addTextLine("It is a class to render generic text.");
	      textArea.addTextLine("It can cope with long lines by measuring how many pixels each word measures and introducing linebreaks when necessary.");

	      textArea.addTextLine("In addition to left justification it can");
	      textArea.setAlign(Align.RIGHT);
	      textArea.addTextLine("justify text to the right");
	      textArea.setAlign(Align.CENTER);
	      textArea.addTextLine("and even center it!");
   }
}

