package slideshow;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * This class represents an image within a Paragraph
 * @author Alex Rivas Romagnoli
 *
 */
public class ImageToken extends Token {
   private PImage image;

   public ImageToken(PImage image_, Style style_, Animation animation_) {
      super(style_, animation_);
      image = image_;
   }
   public PImage getImage() {
      return image;
   }
   @Override
public void measure(PApplet parent) {
      width = image.width;
      ascent = image.height;
      descent = 0;
   }
   @Override
public Box convertToBox(float x, float y, PApplet parent) {
      return new ImageBox(x, y, image, getStyle(), getAnimation(), parent);
   }
}

