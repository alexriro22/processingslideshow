package slideshow;

import java.util.ArrayList;
import java.util.Stack;

import processing.core.PApplet;
import processing.core.PImage;


/**
 * This class is the one that draws some items of an Slide, a Slide could have multiple Writers
 * @author Alex Rivas Romagnoli
 *
 */
public class Writer extends Drawable {
    private float x0,y0,x1,y1;
    private ArrayList<Paragraph> paragraphs = new ArrayList<Paragraph>();
    private ArrayList<Box> boxes = new ArrayList<Box>();
    private Stack<Float> indents = new Stack<Float>();
    private Style style;
    private Align align = Align.LEFT;
    private float firstIndent = 0;
    private String prefix = "";
    private boolean pendingLineBreak = false;
    private PApplet parent;
    
    /**
     * Constructor of the writer
     * @param style_ style of the writer
     * @param x0_ X left top of the writer
     * @param y0_ Y left top of the writer
     * @param width_ width of the writer
     * @param height_ height of the writer
     * @param text_ text of the writer, which will use the style
     * @param parent_ sketch in which we draw
     */
    public Writer(Style style_, float x0_, float y0_, float width_, float height_, String text_, PApplet parent_) {
        this(style_, x0_, y0_, width_, height_, parent_);
        paragraphs.add(new Paragraph(align, indents.peek(), firstIndent));
        int last = paragraphs.size() - 1;
        paragraphs.get(last).add(prefix + text_, new Style(style), new Animation());
    }

    /**
     * Constructor of the writer
     * @param style_ style of the writer
     * @param x0_ X left top of the writer
     * @param y0_ Y left top of the writer
     * @param width_ width of the writer
     * @param height_ height of the writer
     * @param parent_ sketch in which we draw
     */
    public Writer(Style style_, float x0_, float y0_, float width_, float height_, PApplet parent_) {
        style = style_;
        x0 = x0_;
        y0 = y0_;
        x1 = x0_ + width_;
        y1 = y0_ + height_;
        parent = parent_;
        indents.push((float)0);
    }
    
    public void setStyle(Style e) {
    	this.style = e;
    }
    
    
    public int getImagesNumber() {
    	int i = 0;
    	for(Box box : boxes) {
    		if(box instanceof ImageBox) {
    			i++;
    		}
    	}
    	return i;
    }
    
    /**
     * Activate the bold
     */
    public void bold() {
    	
        style.setBold(true);

    }
    
    /**
     * Deactivate the bold
     */
    public void unBold() {

        style.setBold(false);

    }
    
    /**
     * Set the color of the style
     * @param color_ color of the style
     */
    public void setColor(int color) {

        style.setColor(color);

    }
    
    /**
     * Set Size of the style
     * @param size_
     */
    public void setSize(float size) {

        style.setSize(size);

    }
    
    /**
     *  Activate the italic
     */
    public void italic() {

        style.setItalic(true);

    }
    
    /**
     * Deactivate the italic
     */
    public void unItalic() {

        style.setItalic(false);

    }
    
    /**
     * Add an image to the writer
     * @param image image to be add
     * @param x top left X of the image
     * @param y top left Y of the image
     */
    public void addImage(PImage image) {
       addImage(image, new Animation());
    }

    public void addImage(PImage image, Animation animation) {
       if (paragraphs.isEmpty() || pendingLineBreak) {
           paragraphs.add(new Paragraph(align, indents.peek(), firstIndent));
           pendingLineBreak = false;
           if(! prefix.isEmpty()) {
               int last = paragraphs.size() - 1;
               paragraphs.get(last).add(prefix, new Style(style), animation);
           }
       } 
       int last = paragraphs.size() - 1;
       paragraphs.get(last).add(image, new Style(style), animation);
    }
    
    
    /**
     * Add text with the Style of the writer
     * @param text text to be added
     */
    public void addText(String text) {
       addText(text, new Animation());
    }
    public void addText(String text, Animation animation) {
       if (paragraphs.isEmpty() || pendingLineBreak) {
           paragraphs.add(new Paragraph(align, indents.peek(), firstIndent));
           pendingLineBreak = false;
           if(! prefix.isEmpty()) {
               int last = paragraphs.size() - 1;
               paragraphs.get(last).add(prefix, new Style(style), animation);
           }
       } 
       int last = paragraphs.size() - 1;
       paragraphs.get(last).add(text, new Style(style), animation);
    }

    /**
     * Add text with the Style of the writer and with line change
     * @param text text to be added
     */
    public void addTextLine(String text) {
        addTextLine(text, new Animation());
    }

    /**
     * Add text with the Style of the writer and with line change
     * @param text text to be added
     */
    public void addTextLine(String text, Animation animation) {
        addText(text, animation);
        pendingLineBreak = true;
    }
    
    /**
     * Change the line
     */
    public void addTextLine() {
        pendingLineBreak = true;
    }
    
    /**
     * Change the alignment
     * @param align_ alignment of the text
     */
    public void setAlign(Align align_) {
        align = align_; 
        if (!paragraphs.isEmpty() && pendingLineBreak) {
            int last = paragraphs.size() - 1;
            //paragraphs.get(last).setAlign(align);
        }
    }
    
    /**
     * Return the align
     * @return return the align
     */
    public Align getAlign() {
       return align;
    }

    public void indent(float distance) {
       float last = indents.peek();
       indents.push(last+distance);
    }

    public void unIndent() {
       if(indents.size() > 1)
          indents.pop();
    }
    
    public void firstIndent(float distance) {
       firstIndent = distance;
    }
    
    public float getFirstIndent() {
       return firstIndent;
    }

    public void prefix(String prefix_) {
       prefix = prefix_;
    }
    
    public String getPrefix() {
       return prefix;
    }
    
    int minFrame; 
    int maxFrame;
    int currentFrame;
    
    /**
     *Reset the current frame to the minimum
     */
    @Override
	public void reset() {
        currentFrame = 0 ;
    }
    
    /**
     *Returns if the frame is at the maximum point
     *@return  if the frame is at the maximum point or not
     */
    @Override
	public boolean stepForward() {
    	currentFrame++;
        if (currentFrame >= maxFrame)
            return true;
        return false;
        
    }
    
    /**
     *Returns if the frame is at the minimum point
     *@return  if the frame is at the minimum point or not
     */
    @Override
	public boolean stepBackward() {   	
        if (currentFrame <= 0)
            return true;    
        currentFrame--;
        return false;
        
    }
    
   
    
    
    /**
     * reset the maxFrame and the minFrame
     */
    void resetMinMaxFrames() {
        maxFrame= -1;
        minFrame= -1;
    }
    
    /**
     * Updates the MaxFrame  and MinFrame
     * if it is reseted the new value is added, if not we get the max and the min from the MaxFrame/MinFrame  compared to the frame
     * @param frame frame to be added/compared
     */
    void updateMinMaxFrames(Animation animation) {
        int spanMinFrame = animation.getMinFrame();
        int spanMaxFrame = animation.getMaxFrame();
        if (maxFrame == -1)
            maxFrame = spanMaxFrame;
        else
            maxFrame = PApplet.max(maxFrame, spanMaxFrame);
        if (minFrame == -1)
            minFrame = spanMinFrame;
        else
            minFrame = PApplet.min(minFrame, spanMinFrame);
    }
    
    /**
     * Method that add the boxes with the correct position in the X axis and Y axis
     * @param boxesTemp ArrayList with the boxes
     * @param y Y of the box
     * @param a shift the Y axis
     * @param d text descent
     * @param gap space left on the line
     * @param align_ alignment of the text
     */
    public void consolidateBoxes(ArrayList<Box> boxesTemp, float y, float a, float d, float gap, Align align_) {
        for (Box box : boxesTemp) {
            box.setY(y + a);
            if (align_ == Align.RIGHT) {
                box.shiftX(gap);
            }
            else if (align_ == Align.CENTER) {
                box.shiftX(gap / 2);
            }
            else if (align_ == Align.BOTH) {
                //box.shiftX(gap);
            } 
            boxes.add(box);
        }
        boxesTemp.clear();
    }
    
    /**
     * Return the style
     * @return return the style
     */
    public Style getStyle() {
    	return style;
    }
    
    /**
     * Return the paragraphs
     * @return return the paragraphs
     */
    public ArrayList<Paragraph> emptyParagraph() {
    	return paragraphs;
    }
    
    /** Return if the pending line break is true or false
     * @return return if the pending line break is true or false
     */
    public boolean getPendingLineBreak() {
    	
    	return pendingLineBreak;
    }
    
    /**
     * Return the boxes
     * @return return the boxes
     */
    public ArrayList<Box> getBoxes() {
    	
    	return boxes;
    }
    
    /**
     * Parser of the text, this method convert the paragraphs into the boxes with the correct X and Y axis
     * This method also is responsible to add the dot to the text
     */
    private void parse() {
        float x = x0, y = y0, a = 0, d = 0;
        ArrayList<Box> boxesTemp = new ArrayList<Box>();
        boxes.clear();
        
        resetMinMaxFrames();
        paragraphs:
        for (Paragraph paragraph : paragraphs) {
            x = x0 + paragraph.getIndent() + paragraph.getFirstIndent();
            y += a + d;
            if (y + a + d > y1) {
                consolidateBoxes(boxesTemp, y, a, d, x1 - x, paragraph.getAlign());
                break paragraphs;
            }
            a = 0;
            d = 0;
            for (Token token : paragraph.getTokens()) {
                updateMinMaxFrames(token.getAnimation());
                token.measure(parent);
                if (x + token.getWidth() > x1) {
                    consolidateBoxes(boxesTemp, y, a, d, x1 - x, paragraph.getAlign());
                    y += a + d;
                    if (y + a + d > y1) {
                        break paragraphs;
                    }
                    x = x0 + paragraph.getIndent();
                    a = 0;
                    d = 0;
                }
                Box box = token.convertToBox(x, y, parent);
                boxesTemp.add(box);
                a = PApplet.max(a,token.getAscent());
                d = PApplet.max(d,token.getDescent());
                x += token.getWidth()+parent.textWidth(" ");
            }
            consolidateBoxes(boxesTemp, y, a, d, x1 - x, paragraph.getAlign());
            y += .5*(a + d);
        }
    }

    @Override
	public boolean draw() {
        parse();
        boolean relax = true;
        for (Box box : boxes) {
            relax = box.draw(currentFrame) && relax;
            
        }
        return relax;
    }
    
}
