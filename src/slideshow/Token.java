package slideshow;

import processing.core.PApplet;

abstract class Token {
   private Style style;
   private Animation animation;
   protected float width, ascent, descent;

   Token(Style style_, Animation animation_) {
      style = style_;
      animation = animation_;
      width = 0;
      ascent = 0;
      descent = 0;
   }
   /**
    * Return the style of the text
    * @return return the style of the text
    */
   public Style getStyle() {
       return style; 
   }
   
   /**
    * Return the animation of the span
    * @return return the animation of the span
    */
   public Animation getAnimation() {
       return animation;
   }

   public void setupStyle(PApplet parent) {
      if(getStyle().getFont() != null) {
          parent.textFont(getStyle().getFont(), getStyle().getSize());
      }
      else if(getStyle().getSize() >= 0) {
          parent.textSize(getStyle().getSize());
      }          
   }
   public float getWidth() {
      return width;
   } 
   public float getAscent() {
      return ascent;
   }
   public float getDescent() {
      return descent;
   }

   abstract public void measure(PApplet parent); 
   abstract public Box convertToBox(float x, float y, PApplet parent);
}

