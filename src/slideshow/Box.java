package slideshow;

import processing.core.PApplet;

/**
 * Class that draws parsed text in the correct X and Y, each word is a box
 * @author Alex Rivas Romagnoli
 *
 */
abstract public class Box {
    float x,y;
    Style style;
    Animation animation; 
    PApplet parent;
    
    /**
     * Constructor of Box class
     * @param x_ x of the box
     * @param y_ y of the box
     * @param parent_ sketch of the box
     */
    public Box(float x_, float y_, Style style_, Animation animation_, PApplet parent_) {
        x = x_;
        y = y_;
        style = style_;
        animation = animation_;
        parent = parent_;
    }
    
    /**
     *Shifts the current X
     * @param x_ number that we add to the current X
     */
    public void shiftX(float x_) {
        x += x_;
    }
    
    /**
     * Shifts the current Y
     * @param y_ number that we add to the current Y
     */
    public void setY(float y_) {
        y = y_;
    }
    
    /**
     * Returns the current Y
     * @return returns the current Y
     */
    public float getY() {
        return y;
    }
    
    /**
     * Returns the current X
     * @return returns the current X
     */
    public float getX() {
        return x;
    }
    
    /**
     * Draws the Box in the parent Sketch if the current frame passed is less than the current one
     * @param frame_ frame used to compare with the current one
     */
    abstract public boolean draw(int frame_);
}
