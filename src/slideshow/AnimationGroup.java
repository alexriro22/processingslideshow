package slideshow;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;

public class AnimationGroup extends Animation {
   ArrayList<Animation> animations = new ArrayList<Animation>();

   public AnimationGroup(Animation... animations_) {
      for (Animation animation : animations_)
         animations.add(animation);
   }
   @Override
public int getMinFrame() {
      int frame = -1;
      for (Animation animation: animations) {
         if(frame == -1) 
            frame = animation.getMinFrame();
         else
            frame = PApplet.min(frame,animation.getMinFrame());
      }
      return frame;
   }
   @Override
public int getMaxFrame() {
      int frame = -1;
      for (Animation animation: animations) {
         if(frame == -1) 
            frame = animation.getMaxFrame();
         else
            frame = PApplet.max(frame,animation.getMaxFrame());
      }
      return frame;
   }
   @Override
public boolean visible(int frame_) {
      boolean visible = true;
      for (Animation animation: animations) {
         visible = visible && animation.visible(frame_);
      }
      return visible;
   }
   @Override
public boolean active(int frame_, int millis) {
      boolean active = false;
      for (Animation animation: animations) {
         active = animation.active(frame_, millis) || active;
      }
      return active;
   }
   @Override
public float opacity(int frame_, int millis) {
      float opacity = 1;
      for (Animation animation: animations) {
         opacity *= animation.opacity(frame_,millis);
      }
      return opacity;
   }
   @Override
public PVector offset(int frame_, int millis) {
      PVector offset = new PVector(0,0);
      for (Animation animation: animations) {
         offset.add(animation.offset(frame_,millis));
      }
      return offset;
   }
}
