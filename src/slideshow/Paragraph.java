package slideshow;

import java.util.ArrayList;

import processing.core.PImage;

/**
 * Class that contains the spans but with the correct alignment
 * @author Alex Rivas Romagnoli
 *
 */
public class Paragraph {
    private Align align;
    private ArrayList<Token> tokens = new ArrayList<Token>();
    private float indent;
    private float firstIndent;
    
    /**
     * Constructor of the paragraph
     * @param align_ alignment of the paragraph
     */
    public Paragraph(Align align_, float indent_, float firstIndent_) {
        align = align_;
        indent = indent_;
        firstIndent = firstIndent_;
    }
    
    /**
     * Constructor of the paragraph
     * @param text_ text of paragraph
     * @param style_ style of the text 
     * @param animation_ animation of the text
     * @param align_ alignment of the frame
     * @param dot_ true if it has dot or false if not
     */
    public Paragraph(Align align_) {
        align = align_;
    }
    
    /**
     * Return the ArrayList of tokens
     * @return  return the ArrayList of tokens
     */
    public ArrayList<Token> getTokens() {
        return tokens;
    }
    
    /**
     * Set the Align of the Paragraph
     * @param align_
     */
    public void setAlign(Align align_) {
        align = align_;
    }
    
    /**
     * Return the align of the paragraph
     * @return return the align of the paragraph
     */
    public Align getAlign() {
        return align;
    }

    public float getIndent() {
        return indent;
    }
    
    public float getFirstIndent() {
        return firstIndent;
    }
    
    /**
     * Adds a new span
     * @param text_ text of the span
     * @param style_ style of the text
     * @param animation_ animation of the text
     */
    public void add(String text, Style style, Animation animation) {
       for(String word: text.split(" ")) {
          tokens.add(new TextToken(word,style,animation));
       }
    }
    public void add(PImage image_, Style style, Animation animation_) {
        tokens.add(new ImageToken(image_, style, animation_));     
    }
}

