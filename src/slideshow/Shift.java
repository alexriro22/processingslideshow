package slideshow;

import processing.core.PVector;

public class Shift extends Animation {
   int frame;
   PVector from,to;
   int duration;
   int start;

   public Shift(int frame_, float x0, float y0, float x1, float y1, int duration_) {
      frame = frame_;
      from = new PVector(x0,y0);
      to = new PVector(x1,y1);
      duration = duration_;
   }
   @Override
public int getMinFrame() {
      return frame;
   }
   @Override
public int getMaxFrame() {
      return frame+1;
   }
   @Override
public boolean active(int frame_, int millis) {
      return frame_ == frame && start >= 0 && millis < start+duration;
   }
   @Override
public PVector offset(int frame_, int millis) {
      if(frame_ < frame) {
         start = -1;
         return from;
      } else if(frame_ > frame || start >= 0 && millis > start+duration) {
         return to;
      } else if(start < 0) {
         start = millis;
      }
      return PVector.lerp(from,to,(float)(millis-start)/(float)duration);
   }
}
