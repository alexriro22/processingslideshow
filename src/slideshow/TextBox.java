package slideshow;
import processing.core.PApplet;
import processing.core.PVector;

/**
 * Class that draws parsed text in the correct X and Y 
 * @author Alex Rivas Romagnoli
 *
 */
public class TextBox extends Box {
    String text;

    /**
     * Constructor of Box class
     * @param x_ x of the box
     * @param y_ y of the box
     * @param text_ text of the box
     * @param style_ style of the box
     * @param animation_ animation of the box
     * @param parent_ sketch of the box
     */
    public TextBox(float x_, float y_, String text_, Style style_, Animation animation_, PApplet parent_) {
       super(x_,y_,style_,animation_,parent_);
       text = text_;
    }
    
    private int changeAlpha(int color, int opacity) {
       int alpha = (color >> 24) & 0xff;
       alpha = alpha*opacity/256;
       return color & 0x00ffffff | (alpha << 24);
    }
    
    /**
     * Draws the Box in the parent Sketch if the current frame passed is less than the current one
     * @param frame_ frame used to compare with the current one
     */
    @Override
	public boolean draw(int frame_) {
    	
    	System.out.println(frame_);
        if(! animation.visible(frame_)) {
           return true;
        }
        PVector offset = animation.offset(frame_,parent.millis());
        if(style.getFont() != null) {
           parent.textFont(style.getFont(), style.getSize());
        }
        else if(style.getSize() >= 0) {
        	parent.textSize(style.getSize());
        }
        parent.fill(changeAlpha(style.getColor(), (int)(255*animation.opacity(frame_,parent.millis()))));
        parent.text(text, x+offset.x,y+offset.y);
        return ! animation.active(frame_,parent.millis());
    }
}
