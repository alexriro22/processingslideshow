package slideshow;

import processing.core.PVector;

public class Animation {
   public int getMinFrame() {
      return 0;
   }
   public int getMaxFrame() {
      return 0;
   }
   public boolean visible(int frame) {
      return true;
   }
   public boolean active(int frame_, int millis) {
      return false;
   }
   public PVector offset(int frame_, int millis) {
      return new PVector(0,0);
   }
   public float opacity(int frame_, int millis) {
      return (float)1.0;
   }
}
