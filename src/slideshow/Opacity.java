package slideshow;

public class Opacity extends Animation {
   int frame;
   float from,to;
   int duration;
   int start;

   public Opacity(int frame_, float from_, float to_, int duration_) {
      frame = frame_;
      from = from_;
      to = to_;
      duration = duration_;
   }
   @Override
public int getMinFrame() {
      return frame;
   }
   @Override
public int getMaxFrame() {
      return frame+1;
   }
   @Override
public boolean active(int frame_, int millis) {
      return frame_ == frame && start >= 0 && millis < start+duration;
   }
   @Override
public float opacity(int frame_, int millis) {
      if(frame_ < frame) {
         start = -1;
         return from;
      } else if(frame_ > frame || start >= 0 && millis > start+duration) {
         return to;
      } else if(start < 0) {
         start = millis;
      }
      return from+(float)(millis-start)/(float)duration*(to-from);
   }
}
