package slideshow;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

/**
 * Class that shows an image
 * @author Alex Rivas Romagnoli
 *
 */
public class ImageBox extends Box {
    private PImage image;
    
    /**
     * Constructor of the ImageBox
     * @param name_ name of the image to be shown
     * @param parent_ skech in which the image is shown
     */
    public ImageBox(float x, float y, PImage image_, Style style, Animation animation, PApplet parent) {
       super(x,y,style,animation,parent);
       image = image_;
    }

    
    /**
     * Draws the image if the frame_ is less than the current frame
     * @param frame_ frame to compare with the current frame
     */
    @Override
	public boolean draw(int frame_){
        if(! animation.visible(frame_)) {
           return true;
        }
        PVector offset = animation.offset(frame_,parent.millis());
        parent.tint(255, (int)(255*animation.opacity(frame_,parent.millis())));
        parent.image(image, x+offset.x,y+offset.y-image.height);
        parent.noTint();
        return ! animation.active(frame_,parent.millis());
    }   
}
