package slideshow;

public class Stage extends Animation {
   int from, to;
   boolean hasTo;
   public Stage(int from_) {
      hasTo = false;
      from = from_;
   }
   public Stage(int from_, int to_) {
      hasTo = true;
      from = from_;
      to = to_;
   }
   @Override
public int getMinFrame() {
      return from;
   }
   @Override
public int getMaxFrame() {
      return hasTo ? to+1 : from;
   }
   @Override
public boolean visible(int frame) {
      return from <= frame && (!hasTo || frame <= to);
   }
}
