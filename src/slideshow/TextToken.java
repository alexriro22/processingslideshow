package slideshow;

import processing.core.PApplet;

class TextToken extends Token {
   public String word;
   public TextToken(String word_, Style style, Animation animation) {
      super(style,animation);
      word = word_;
   }
   @Override
public void measure(PApplet parent) {
      setupStyle(parent);
      width = parent.textWidth(word);
      ascent = parent.textAscent();
      descent = parent.textDescent();
   }
   @Override
public Box convertToBox(float x, float y, PApplet parent) {
      return new TextBox(x, y, word, getStyle(), getAnimation(), parent);
   }
}

